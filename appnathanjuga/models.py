from django.db import models


# Create your models here.


class matkul(models.Model):
    nama = models.CharField(max_length=50, null=True)
    dosen = models.CharField(max_length=50, null=True)
    jumlah = models.IntegerField(null=True)
    deskripsi = models.CharField(max_length=50, null=True)
    semester = models.CharField(max_length=50, null=True)
    ruang = models.CharField(max_length=50, null=True)
