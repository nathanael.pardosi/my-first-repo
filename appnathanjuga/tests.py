from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.test import client
from django.apps import apps
from .views import Form
from .apps import AppnathanjugaConfig


class TestApp(TestCase):
    def test_app_is_exist(self):
        self.assertEqual(AppnathanjugaConfig.name, 'appnathanjuga')
        self.assertEqual(apps.get_app_config(
            'appnathanjuga').name, 'appnathanjuga')


class TestIndex(TestCase):
    def test_url_bantuan_is_exist(self):
        response = Client().post('/app/Accordion/')
        self.assertEqual(response.status_code, 200)

    def test_index_template(self):
        response = Client().get('/app/Accordion/')
        self.assertTemplateUsed(response, 'accordion.html')
