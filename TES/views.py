from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import PostForm
from .models import matkul


def home(request):
    return render(request, 'main/home.html')


def profile(request):
    return render(request, 'main/profile.html')


def portofolio(request):
    return render(request, 'main/portofolio.html')


def susun(request):
    post_form = PostForm(request.POST or None)
    if request.method == 'POST':
        # cara ambil data dari input user ke database dg validasi
        if post_form.is_valid():
            post_form.save()
        return HttpResponseRedirect("/main/susun")
    context = {
        'pageTitle': 'Create Form',
        'post_form': post_form,
    }
    return render(request, 'main/susun.html', context)


def jadwal(request):
    posts = matkul.objects.all()
    context = {
        'heading': 'Post',
        'contents': 'List Post',
        'posts': posts,
    }
    return render(request, 'main/jadwal.html', context)  # context


def update(request, update_id):
    akun_update = matkul.objects.get(id=update_id)
    data = {
        'Mata_Kuliah': akun_update.Mata_Kuliah,
        'Notes': akun_update.Notes,
        'Dosen': akun_update.Dosen,
        'Ruangan': akun_update.Ruangan,
        'Jumlah_SKS': akun_update.Jumlah_SKS,
        'semester': akun_update.semester,
    }
    post_form = PostForm(request.POST or None,
                         initial=data, instance=akun_update)
    if request.method == 'POST':
        # cara ambil data dari input user ke database dg validasi
        if post_form.is_valid():
            post_form.save()
        return HttpResponseRedirect("/main/jadwal")
    context = {
        'pageTitle': 'Update Form',
        'post_form': post_form,
    }
    return render(request, 'main/susun.html', context)


def delete(request, delete_id):
    matkul.objects.filter(id=delete_id).delete()
    return HttpResponseRedirect("main/jadwal")
