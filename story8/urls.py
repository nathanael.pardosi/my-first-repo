from django.urls import path, include
from .views import fungsi_data, fungsi_formulir
from django.views.generic import TemplateView

urlpatterns = [
    path('ajax/',  fungsi_formulir, name="ajax"),
    path('data/',  fungsi_data, name="data")

]
