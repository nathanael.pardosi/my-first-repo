from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from .views import fungsi_formulir
from .apps import Story8Config


class Story8(TestCase):

    def test_page(self):
        response = Client().get("/story8/ajax/")
        self.assertEqual(response.status_code, 200)

    def test_page_template(self):
        response = Client().get('/story8/ajax/')
        self.assertTemplateUsed(response, 'cari.html')

    def test_json_url(self):
        response = Client().get('/story8/data/')
        self.assertEqual(response.status_code, 200)

    def test_header(self):
        request = HttpRequest()
        response = fungsi_formulir(request)
        html_response = response.content.decode('utf8')
        self.assertIn("It's my life", html_response)

    def test_app(self):
        self.assertIn(Story8Config.name, 'story8')
