from django.shortcuts import render, HttpResponseRedirect
from .models import matkul
from .forms import PostForm


def fungsi(request):
    return render(request, 'index.html')


def Form(request):
    # isimatkul = matkul.objects.all()
    # response = {
    #     'matkuls': isimatkul
    # }
    return render(request, "accordion.html")


def Untukisi(request):
    form = PostForm
    if request.method == 'POST':
        form = PostForm(request.POST)

        matkul.objects.create(
            nama=request.POST['nama'],
            dosen=request.POST['dosen'],
            jumlah=request.POST['jumlah'],
            deskripsi=request.POST['deskripsi'],
            semester=request.POST['semester'],
            ruang=request.POST['ruang']
        )

        return HttpResponseRedirect('Form')

    response = {
        'form': form
    }
    return render(request, "request.html", response)


def hapusisi(request, id_matkul):
    matkul.objects.filter(id=id_matkul).delete()
    return HttpResponseRedirect('/Form')
