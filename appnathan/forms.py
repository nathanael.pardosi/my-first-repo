from django import forms
from .models import matkul  # import model dari models.py


class PostForm(forms.Form):
    nama = forms.CharField()
    dosen = forms.CharField()
    jumlah = forms.IntegerField()
    deskripsi = forms.CharField()
    semester = forms.CharField()
    ruang = forms.CharField()

    # 'class' : 'form-control' digunakan untuk mengisi sesuai bootstrap
