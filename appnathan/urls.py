from django.urls import path, include
from .views import fungsi, Form, Untukisi, hapusisi
from django.views.generic import TemplateView

urlpatterns = [
    path('', fungsi),
    path('Portofolio', TemplateView.as_view(template_name="Portofolio.html")),
    path('Form', Form),
    path('Request', Untukisi, name='Request'),
    path('delete/<id_matkul>', hapusisi, name='delete')

]
