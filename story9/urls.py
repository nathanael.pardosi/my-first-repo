from django.urls import path
from .views import index, log_in, log_out, sign_up


appname = 'Story8'

urlpatterns = [
    path('signup/', sign_up, name='signup'),
    path('login/', log_in, name='login'),
    path('logout/', log_out, name='logout'),

]
